# Wago PFC Configuration Repository

This repository contains configuration files for the Wago PFC (750-8212) controllers used in various SectionStar subsystems.

## Prerequisites

### Required Software (Licensed)
- Ecockpit `v11.3.1.0`
- Wago I/O Check `v3.30.0`
- Wago Ethernet Settings `v6.18.4`

### Hardware Requirements
- Use the `CP-H-23` laptop (all required software is pre-installed with valid licenses)

## Installation Steps

1. Download the configuration file for your system:
   - [V1 Wago Configuration](https://bitbucket.org/clarapath/wagosoftware/raw/bfd78a914bed5a16d21e4745524bace30e858b62/src/V1-Wago-Config.zip)
   - [V2 Wago Configuration](https://bitbucket.org/clarapath/wagosoftware/raw/7e4309c1f99471ca838a60f562662a600f4662bf/src/V2-Wago-Config.zip)

2. Connect to the Wago PFC following the [Wago Setup Guide](https://clarapath.atlassian.net/wiki/spaces/SECTIONSTA/pages/1852932148/WAGO)

3. Open e!COCKPIT, select the `.ecp` file and download the program to the PFC following the setup guide instructions

## References

- [V1 Electrical Box Diagram](docs/V1-Wago-Electrical-Diagram.pdf)
- [V2 Electrical Box Diagram](docs/V2-Wago-Electrical-Diagram.pdf)

